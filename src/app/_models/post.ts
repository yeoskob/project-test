export interface post {
  userId: string;
  id: string;
  title: string;
  body: string;
  username:string;
}

export interface user {
  id: string;
  name: string;
  username: string;
  email: string;
}