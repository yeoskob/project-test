import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { post } from "../_models/post";

@Injectable({providedIn: 'root'})
export class PostService {
  constructor(private httpClient: HttpClient) { }
  
  getPosts(){
    return this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/posts`)
    .pipe()
  }

  getusers(){
    return this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/users`)
    .pipe()
  }



  getcomment(postid:string){
    return this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/posts/${postid}/comments`)
    .pipe()
  }


  getPostsId(postid:string){
    return this.httpClient.get<any>(`https://jsonplaceholder.typicode.com/posts/${postid}`)
    .pipe()
  }

}