import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, filter, map, share, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Router } from  '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient, 
    private router: Router, 

  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public get currentUserValue(): User {
    return this.currentUserSubject.value;
}


  public login(username: string, password: string){
   const getUsers$ = this.getUsers(username)
   .pipe(
    //  tap((x:any)=>console.log(`x response`, x)),
     tap((x:any) => {
       x.forEach((e:any) => {
         if(e.username === username && e.username === password){
          localStorage.setItem('currentuser', JSON.stringify(e));
          localStorage.setItem('alluser', JSON.stringify(x));
          this.currentUserSubject.next(e);
          this.router.navigateByUrl('/dashboard');

         }
       });
     })
   )
   getUsers$.subscribe()
  }

  getUsers(userInfo:string){
    return this.http.get<any>(`https://jsonplaceholder.typicode.com/users`)
    .pipe(
    )
  }

  public isLoggedIn(){
    return localStorage.getItem('currentuser') !== null;
    

  }

  public logout(){
    localStorage.removeItem('currentuser');
    localStorage.removeItem('alluser');

  }
}