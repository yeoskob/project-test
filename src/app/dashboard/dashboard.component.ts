import { DecimalPipe } from '@angular/common';
import { Component, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin, from, Observable } from 'rxjs';
import { concatMap, startWith } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { User } from '../user';
import { post, user } from "../_models/post";
import { PostService } from "../_services/post.service";
import { catchError, filter, map, share, switchMap, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  currentUser: string;
  userprofile:any;
  postResponse!: any;
  listUser!:any;
  listcomment!:any;
  merged!:any;
  constructor(
    private authService: AuthService, 
    private router: Router,
    
    private postService: PostService
    ) { 
    this.currentUser = this.authService.currentUserValue.username;
    this.userprofile = this.authService.currentUserValue;
  }


  page = 1;
  pageSize = 10;
  collectionSize =0;
  listItem!: any[];
  showdetail: boolean = false;
  showdetailcomment: boolean = false;
  isShowProfile: boolean = false;

  postid:any;
  commentsPostid:any;

  refreshCountries() {
    this.listItem = this.postResponse
      .map((list:any, i:any) => ({id: i + 1, ...list}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getcomment(postid:string, post:any){
    const getcomment$ = this.postService.getcomment(postid)
    .pipe(
      tap((x:any)=>{            
        x.forEach((elc:any) => {
          if(postid == elc.postId){
            post["totalComment"] = "tes"
          }
        });
      }),
      // tap((x:any)=> )
    )
    getcomment$.subscribe()       

  }

  ngOnInit(): void {


    
    const getUsers$ = this.postService.getusers()
    .pipe(
      map((x:any)=>{
        return this.listUser = x;
      }),

    )



    const getPosts$ = this.postService.getPosts()
    .pipe(
     
      map((x:any)=>{
        return this.postResponse = x;
      }),
      switchMap((x:any)=>getUsers$),
      tap((x:any)=> {
        this.postResponse.forEach((elp:any) => {
          this.listUser.forEach((elu:any) => {
            if(elp.userId == elu.id){
              elp["username"]= elu.username
              // console.log(elu.username)    
            }
          });
          // this.getcomment(elp.id,this.postResponse)

        });
        this.collectionSize = this.postResponse.length;
        this.refreshCountries();
      }),
    )
    getPosts$.subscribe()





    

  }


  detail(postId:string,username:string)
  {
    this.showdetail = !this.showdetail

    if(postId !== 'temp'){
      const getPostid$ = this.postService.getPostsId(postId)
      .pipe(      
        tap((x:any)=> console.log('postid', x)),
        tap((x:any)=>{
          this.postid = x; 
          this.postid["username"] = username;
        }),
        switchMap((x:any)=>this.postService.getcomment(postId)),
        tap((x:any)=> this.commentsPostid = x),
      )
      getPostid$.subscribe() 
    }
      

  }

  showcomment(postid:string){
    console.log(postid)
    this.showdetailcomment = !this.showdetailcomment

  }

  showProfile(){
    console.log('show profile')
    this.isShowProfile = !this.isShowProfile

  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}
