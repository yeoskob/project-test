import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'tableFilter'})
export class TableFilter implements PipeTransform {

    transform(list: any[], filterText: string): any {
        return list ? list.filter(item => item.name.search(new RegExp(filterText, 'i')) > -1) : [];
    }
}